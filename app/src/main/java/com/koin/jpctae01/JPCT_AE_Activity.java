package com.koin.jpctae01;

import androidx.appcompat.app.AppCompatActivity;

import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.MotionEvent;

import com.threed.jpct.Camera;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Light;
import com.threed.jpct.Logger;
import com.threed.jpct.Object3D;
import com.threed.jpct.Primitives;
import com.threed.jpct.RGBColor;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.World;
import com.threed.jpct.util.BitmapHelper;
import com.threed.jpct.util.MemoryHelper;

import java.lang.reflect.Field;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;

public class JPCT_AE_Activity extends AppCompatActivity {

    private static JPCT_AE_Activity master = null;

    private GLSurfaceView mGLView;

    private MyRenderer renderer = null;

    private FrameBuffer fb = null;

    private World world = null;

    // 設定背景顏色
    private RGBColor back = new RGBColor(150,50,100);
    private float touchTurn = 0;
    private float touchTurnUp = 0;
    private float xpos = -1;
    private float ypos = -1;

    private Object3D cube = null;

    // 每秒幀數
    private int fps = 0;

    // 光照類
    private Light sun = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Logger.log("onCreate");

        if (master != null) {
            copy(master);
        }

        super.onCreate(savedInstanceState);

        mGLView = new GLSurfaceView(this);

        mGLView.setEGLConfigChooser(new GLSurfaceView.EGLConfigChooser() {
            @Override
            public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
                int[] attributes = new int[] { EGL10.EGL_DEPTH_SIZE, 16,
                        EGL10.EGL_NONE };
                EGLConfig[] configs = new EGLConfig[1];
                int[] result = new int[1];
                egl.eglChooseConfig(display, attributes, configs, 1, result);
                return configs[0];
            }
        });

        renderer = new MyRenderer();

        mGLView.setRenderer(renderer);

        setContentView(mGLView);

    }

    @Override
    protected void onPause() {
        super.onPause();
        mGLView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGLView.onResume();
    }

    // 重寫onStop()
    @Override
    protected void onStop() {
        super.onStop();
    }

    private void copy(Object src) {
        try {
            // 打印日志
            Logger.log("Copying data from master Activity!");
            // 返回一個數組，其中包含目前這個類的的所有字段的Filed對象
            Field[] fs = src.getClass().getDeclaredFields();
            // 遍歷fs數組
            for (Field f : fs) {
                // 嘗試設置無障礙標志的值。標志設置為false將使訪問檢查，設置為true，將其禁用。
                f.setAccessible(true);
                // 將取到的值全部裝入當前類中
                f.set(this, f.get(src));
            }
        } catch (Exception e) {
            // 拋出運行時異常
            throw new RuntimeException(e);
        }
    }


    public boolean onTouchEvent(MotionEvent me) {
        // 按鍵開始
        if (me.getAction() == MotionEvent.ACTION_DOWN) {
            // 保存按下的初始x,y位置於xpos,ypos中
            xpos = me.getX();
            ypos = me.getY();
            return true;
        }
        // 按鍵結束
        if (me.getAction() == MotionEvent.ACTION_UP) {
            // 設置x,y及旋轉角度為初始值
            xpos = -1;
            ypos = -1;
            touchTurn = 0;
            touchTurnUp = 0;
            return true;
        }
        if (me.getAction() == MotionEvent.ACTION_MOVE) {
            // 計算x,y偏移位置及x,y軸上的旋轉角度
            float xd = me.getX() - xpos;
            float yd = me.getY() - ypos;
            // Logger.log("me.getX() – xpos———–>>"
            // + (me.getX() – xpos));
            xpos = me.getX();
            ypos = me.getY();
            Logger.log("xpos————>>" + xpos);
            // Logger.log("ypos————>>" + ypos);
            // 以x軸為例，鼠標從左向右拉為正，從右向左拉為負
            touchTurn = xd / -100f;
            touchTurnUp = yd / -100f;
            Logger.log("touchTurn————>>" + touchTurn);
            // Logger.log("touchTurnUp————>>" + touchTurnUp);
            return true;
        }
        // 每Move一下休眠毫秒
        try {
            Thread.sleep(15);
        } catch (Exception e) {
            // No need for this…
        }
        return super.onTouchEvent(me);
    }


    class MyRenderer implements GLSurfaceView.Renderer {

        // 當前系統的毫秒數
        private long time = System.currentTimeMillis();
        // 是否停止
        private boolean stop = false;

        @Override
        public void onSurfaceCreated(GL10 gl, EGLConfig config) {

        }

        @Override
        public void onSurfaceChanged(GL10 gl, int w, int h) {
            // 如果FrameBuffer不為NULL,釋放fb所占資源
            if (fb != null) {
                fb.dispose();
            }
            // 創建一個寬度為w,高為h的FrameBuffer
            fb = new FrameBuffer(gl, w, h);
            Logger.log(master + "");
            // 如果master為空
            if (master == null) {
                // 實例化World對象
                world = new World();
                // 設置瞭環境光源強度。設置此值是負的整個場景會變暗，而為正將照亮瞭一切。
                world.setAmbientLight(20, 20, 20);
                // 在World中創建一個新的光源
                sun = new Light(world);
                // 設置光照強度
                sun.setIntensity(250, 250, 250);
                // 創建一個紋理
                // 構造方法Texture(Bitmap image)
                // static Bitmap rescale(Bitmap bitmap, int width, int height)
                // static Bitmap convert(Drawable drawable)
                Texture texture = new Texture(BitmapHelper.rescale(
                        BitmapHelper.convert(getResources().getDrawable(
                                R.drawable.i1)), 64, 64));
                // TextureManager.getInstance()取得一個Texturemanager對象
                // addTexture("texture",texture)添加一個紋理
                TextureManager.getInstance().addTexture("texture", texture);
                // Object3D對象開始瞭:-)
                // Primitives提供瞭一些基本的三維物體，假如你為瞭測試而生成一些對象或為
                // 其它目的使用這些類將很明智，因為它即快速又簡單，不需要載入和編輯。
                // 調用public static Object3D getCube(float scale) scale:角度
                // 返回一個立方體
                cube = Primitives.getCube(10);
                // 以紋理的方式給對象所有面"包裝"上紋理
                cube.calcTextureWrapSpherical();
                // 給對象設置紋理
                cube.setTexture("texture");
                // 除非你想在事後再用PolygonManager修改,否則釋放那些不再需要數據的內存
                cube.strip();
                // 初始化一些基本的對象是幾乎所有進一步處理所需的過程。
                // 如果對象是"準備渲染"(裝載，紋理分配，安置，渲染模式設置，
                // 動畫和頂點控制器分配),那麼build()必須被調用，
                cube.build();
                // 將Object3D對象添加到world集合
                world.addObject(cube);
                // 該Camera代表瞭Camera/viewer在當前場景的位置和方向，它也包含瞭當前視野的有關信息
                // 你應該記住Camera的旋轉矩陣實際上是應用在World中的對象的一個旋轉矩陣。
                // 這一點很重要，當選擇瞭Camera的旋轉角度，一個Camera(虛擬)圍繞w旋轉和通過圍繞World圍繞w旋轉、
                // 將起到相同的效果，因此，考慮到旋轉角度，World圍繞camera時，camera的視角是靜態的。假如你不喜歡
                // 這種習慣，你可以使用rotateCamera()方法
                Camera cam = world.getCamera();
                // 以50有速度向後移動Camera（相對於目前的方向）
                cam.moveCamera(Camera.CAMERA_MOVEOUT, 50);
                // cub.getTransformedCenter()返回對象的中心
                // cam.lookAt(SimpleVector lookAt))
                // 旋轉這樣camera以至於它看起來是在給定的world-space 的位置
                cam.lookAt(cube.getTransformedCenter());
                // SimpleVector是一個代表三維矢量的基礎類，幾乎每一個矢量都
                // 是用SimpleVector或者至少是一個SimpleVector變體構成的(有時由於
                // 某些原因比如性能可能會用(float x,float y,float z)之類)。
                SimpleVector sv = new SimpleVector();
                // 將當前SimpleVector的x,y,z值設為給定的SimpleVector(cube.getTransformedCenter())的值
                sv.set(cube.getTransformedCenter());
                // Y方向上減去100
                sv.y -= 100;
                // Z方向上減去100
                sv.z -= 100;
                // 設置光源位置
                sun.setPosition(sv);
                // 強制GC和finalization工作來試圖去釋放一些內存，同時將當時的內存寫入日志，
                // 這樣可以避免動畫不連貫的情況，然而，它僅僅是減少這種情況發生的機率
                MemoryHelper.compact();
                // 如果master為空,使用日志記錄且設master為HelloWorld本身
                if (master == null) {
                    Logger.log("Saving master Activity!");
                    master = JPCT_AE_Activity.this;
                }
            }
        }

        @Override
        // 繪制到當前屏幕哦:-D
        public void onDrawFrame(GL10 gl) {
            try {
                // 如果stop為true
                if (!stop) {
                    // 如果touchTurn不為0,向Y軸旋轉touchTure角度
                    if (touchTurn != 0) {
                        // 旋轉物體的旋轉繞Y由給定矩陣W軸角（弧度順時針方向為正值）,應用到對象下一次渲染時。
                        cube.rotateY(touchTurn);
                        // 將touchTurn置0
                        touchTurn = 0;
                    }
                    if (touchTurnUp != 0) {
                        // 旋轉物體的旋轉圍繞x由給定角度寬（弧度，逆時針為正值）軸矩陣,應用到對象下一次渲染時。
                        cube.rotateX(touchTurnUp);
                        // 將touchTureUp置0
                        touchTurnUp = 0;
                    }
                    // 用給定的顏色(back)清除FrameBuffer
                    fb.clear(back);
                    // 變換和燈光所有多邊形
                    world.renderScene(fb);
                    // 繪制
                    world.draw(fb);
                    // 渲染圖像顯示
                    fb.display();
                    // 記錄FPS
                    if (System.currentTimeMillis() - time >= 1000) {
                        // Logger.log(fps + "fps");
                        fps = 0;
                        time = System.currentTimeMillis();
                    }
                    fps++;
                    // 如果stop為false,釋放FrameBuffer
                } else {
                    if (fb != null) {
                        fb.dispose();
                        fb = null;
                    }
                }
                // 當出現異常，打印異常信息
            } catch (Exception e) {
                Logger.log(e, Logger.MESSAGE);
            }
        }

        // 停止
        public void stop() {
            stop = true;
        }

    }


}